package fr.afpa.calendar;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import fr.afpa.calendar.utils.Constant;
import fr.afpa.calendar.utils.ConverterDate;
import fr.afpa.calendar.utils.HoursMinutes;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView textViewDateTitle;
    private TextView textViewDate1;
    private TextView textViewDate2;
    private TextView textViewDate3;
    private TextView textViewDate4;
    private TextView textViewDate5;
    private TextView textViewDate6;
    private TextView textViewDate7;
    private GridView gridViewHours;
    private TextView textViewFooterDate;

    private Calendar myCalendar;
    private Date today;
    private ArrayAdapter<String> adapter;
    private List<String> hoursList = new ArrayList<>();
    private Calendar currentCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewDateTitle = findViewById(R.id.textViewDateTitle);
        textViewDate1 = findViewById(R.id.textViewDate1);
        textViewDate2 = findViewById(R.id.textViewDate2);
        textViewDate3 = findViewById(R.id.textViewDate3);
        textViewDate4 = findViewById(R.id.textViewDate4);
        textViewDate5 = findViewById(R.id.textViewDate5);
        textViewDate6 = findViewById(R.id.textViewDate6);
        textViewDate7 = findViewById(R.id.textViewDate7);
        gridViewHours = findViewById(R.id.gridViewHours);
        textViewFooterDate = findViewById(R.id.textViewFooterDate);

        myCalendar = Calendar.getInstance();
        today = myCalendar.getTime();

        setDate(Constant.CALENDAR_CURRENT);

        // gridView
        // hoursList.addAll(HoursMinutes.getHours(myCalendar.get(Calendar.HOUR_OF_DAY)));
        hoursList.addAll(HoursMinutes.getHours(Constant.CALENDAR_START_HOUR));

        adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, hoursList);
        gridViewHours.setAdapter(adapter);
        gridViewHours.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String hourMinute = hoursList.get(position);

                textViewFooterDate.setText(ConverterDate.getFrench2(currentCalendar)+" - " +hourMinute);
            }
        });

        // onClick textViewDate
        textViewDate1.setOnClickListener(MainActivity.this);
        textViewDate2.setOnClickListener(MainActivity.this);
        textViewDate3.setOnClickListener(MainActivity.this);
        textViewDate4.setOnClickListener(MainActivity.this);
        textViewDate5.setOnClickListener(MainActivity.this);
        textViewDate6.setOnClickListener(MainActivity.this);
        textViewDate7.setOnClickListener(MainActivity.this);
    }

    private void setDate(int type) {

        if(type == Constant.CALENDAR_CURRENT) {
            // start on monday
            myCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        } else if(type == Constant.CALENDAR_PREVIOUS) {
            myCalendar.add(Calendar.DATE, -14);
        }

        textViewDateTitle.setText(ConverterDate.getFrench(myCalendar));

        setDateByDay(textViewDate1);
        setDateByDay(textViewDate2);
        setDateByDay(textViewDate3);
        setDateByDay(textViewDate4);
        setDateByDay(textViewDate5);
        setDateByDay(textViewDate6);
        setDateByDay(textViewDate7);
    }

    private void setDateByDay(TextView textViewDate) {
        textViewDate.setText(String.valueOf(myCalendar.get(Calendar.DATE)));
        textViewDate.setTag(myCalendar.clone());

        if(today.equals(myCalendar.getTime())) {
            if(currentCalendar == null) {
                currentCalendar = (Calendar) textViewDate.getTag();
            }

            // set today color
            textViewDate.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorDark));
            textViewDate.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
        } else {
            textViewDate.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
            textViewDate.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorDark));
        }

        myCalendar.add(Calendar.DATE, 1);
    }

    public void previous(View view) {
        setDate(Constant.CALENDAR_PREVIOUS);
        textViewDate7.performClick();
        textViewFooterDate.setText(null);
    }

    public void next(View view) {
        setDate(Constant.CALENDAR_NEXT);
        textViewDate1.performClick();
        textViewFooterDate.setText(null);
    }

    public void submit(View view) {
    }

    @Override
    public void onClick(View v) {
        TextView textViewCurrent = (TextView) v;
        textViewDate1.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewDate2.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewDate3.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewDate4.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewDate5.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewDate6.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewDate7.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        textViewCurrent.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorDark));

        currentCalendar = (Calendar) textViewCurrent.getTag();
        textViewFooterDate.setText(null);
    }
}

package fr.afpa.calendar.utils;

import java.util.Collection;

public class Constant {
    public static final int CALENDAR_CURRENT = 0;
    public static final int CALENDAR_NEXT = 1;
    public static final int CALENDAR_PREVIOUS = 2;

    public static final int CALENDAR_START_HOUR = 9;
    public static final int CALENDAR_MINUTE_INTERVAL = 15;
}

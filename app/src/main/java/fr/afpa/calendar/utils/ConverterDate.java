package fr.afpa.calendar.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ConverterDate {

    public static String getFrench(Calendar myCalendar) {
        SimpleDateFormat format1 = new SimpleDateFormat("MMM yyyy", Locale.FRENCH);
        String dateConverted = format1.format(myCalendar.getTime());
        return dateConverted.substring(0, 1).toUpperCase() + dateConverted.substring(1);
    }

    public static String getFrench2(Calendar myCalendar) {
        SimpleDateFormat format1 = new SimpleDateFormat("dd MMM yyyy", Locale.FRENCH);
        String dateConverted = format1.format(myCalendar.getTime());
        return dateConverted.substring(0, 1).toUpperCase() + dateConverted.substring(1);
    }

}

package fr.afpa.calendar.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HoursMinutes {

    public static List<String> getHours(int hour) {
        int minute = 0;
        List<String> hourMinuteList = new ArrayList<>();

        while(hour <= 23) {

            while (minute <= 59) {
                hourMinuteList.add(String.format("%02d:%02d", hour, minute));

                minute = minute + Constant.CALENDAR_MINUTE_INTERVAL;
            }
            minute = 0;

            hour++;
        }
        return hourMinuteList;
    }

}
